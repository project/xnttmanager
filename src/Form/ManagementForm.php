<?php

namespace Drupal\xnttmanager\Form;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

/**
 * Class ManagementForm.
 *
 * Provides a simple form to inspect or batch process external entities.
 *
 * Selection du type d'external entity
 * Bouton de test de chargement de toutes les entities
 * Bouton de test de chargement+sauveguarde de toutes les external entities
 * Generation de stats sur les entities synchronisées: nombre dispo, nouvelles,
 *   orphœlines
 *
 * @see \Drupal\Core\Form\FormBase
 */
class ManagementForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;
  /**
   * The field type plugin manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypePluginManager;
  /**
   * The field mapper plugin manager.
   *
   * @var \Drupal\external_entities\FieldMapper\FieldMapperManager
   */
  protected $fieldMapperPluginManager;
  /**
   * The storage client plugin manager.
   *
   * @var \Drupal\external_entities\StorageClient\ExternalEntityStorageClientManager
   */
  protected $storageClientPluginManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->entityFieldManager = $container->get('entity_field.manager');
    $instance->fieldTypePluginManager = $container->get('plugin.manager.field.field_type');
    $instance->fieldMapperPluginManager = $container->get('plugin.manager.external_entities.field_mapper');
    $instance->storageClientPluginManager = $container->get('plugin.manager.external_entities.storage_client');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $page = $form_state->get('page');
    if (!empty($page) && ('import' == $page)) {
      return $this->buildImportForm($form, $form_state);
    }

    $xntt_type_list = xnttmanager_get_external_entity_type_list();
    $form['xntt_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Select an external entity type'),
      '#description' => $this->t('Only entities with required fields mapped will be shown here.'),
      '#options' => $xntt_type_list,
      '#empty_value' => '',
      '#sort_options' => TRUE,
    ];

    $form['inspect_group'] = [
      '#type' => 'details',
      '#title' => $this->t('Mapping inspection'),
    ];

    $form['inspect_group']['xntt_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('External Entity identifier (optional)'),
      '#description' => $this->t('Enter the identifier of a specific external entity you want to inspect. Leave empty to select the first available one.'),
      '#required' => FALSE,
    ];

    $form['inspect_group']['actions'] = [
      '#type' => 'actions',
      'inspect' => [
        '#type' => 'submit',
        '#name' => 'inspect',
        '#value' => $this->t('Inspect'),
        '#submit' => [[$this, 'submitInspectForm']],
      ],
    ];
    // @todo Add a button to autogenerate mapping with the simple field mapper.
    $form['batch_group'] = [
      '#type' => 'details',
      '#title' => $this->t('Batch processing'),
      '#description' => $this->t('When batch processing, all external entities of the selected type are
        loaded to check if they load fine and a global report is generated.
        Other additional actions can also be performed by checking the checkboxes below.'),
    ];

    $form['batch_group']['xntt_save'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Save external entities as well'),
      '#description' => $this->t('Check to save selected external entities during batch processing. It might be useful for mass data conversion with xnttmulti.'),
      '#default_value' => FALSE,
    ];

    $form['batch_group']['xntt_annotate'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Annotate external entities'),
      '#description' => $this->t('Check to add missing annotation to selected external entities during batch processing. Note: the selected external entity type must have an annotation content set in its settings.'),
      '#default_value' => FALSE,
    ];

    $form['batch_group']['actions'] = [
      '#type' => 'actions',
      'process' => [
        '#type' => 'submit',
        '#name' => 'process',
        '#value' => $this->t('Batch process'),
        '#submit' => [[$this, 'submitBatchForm']],
      ],
    ];

    $form['config_group'] = [
      '#type' => 'details',
      '#title' => $this->t('Config (experimental)'),
      '#description' => $this->t('You can export or import external entities fields, field mapping and storage configuration to and from YAML files.'),
    ];

    $form['config_group']['xntt_config_file'] = [
      '#type' => 'file',
      '#title' => $this->t('Config file to import'),
      '#multiple' => FALSE,
      '#upload_validators' => [
        'FileExtension' => [
          'extensions' => 'yml yaml json',
        ],
      ],
    ];

    $form['config_group']['actions'] = [
      '#type' => 'actions',
    ];
    $form['config_group']['actions']['export'] = [
      '#type' => 'submit',
      '#name' => 'export',
      '#value' => $this->t('Export config'),
      '#submit' => [[$this, 'submitExportForm']],
    ];

    $form['config_group']['actions']['import'] = [
      '#type' => 'submit',
      '#name' => 'import',
      '#value' => $this->t('Import config on selected external entity type'),
      '#submit' => [[$this, 'submitPreImportForm']],
      '#validate' => [[$this, 'validatePreImportForm']],
    ];

    $form['config_group']['actions']['create_import'] = [
      '#type' => 'submit',
      '#name' => 'create_import',
      '#value' => $this->t('New external entity type from import'),
      '#submit' => [[$this, 'submitPreCreateImportForm']],
      '#validate' => [[$this, 'validatePreImportForm']],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xnttmanager_management_form';
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    if (empty($form_state->getValue('xntt_type'))) {
      $form_state->setErrorByName(
        'xntt_type',
        $this->t('You must select an external entity type.')
      );
    }

    if ($form_state->getValue('xntt_annotate')) {
      // Check if external entity type has an annotation.
      $entity_type = \Drupal::service('entity_type.manager')
        ->getStorage('external_entity_type')
        ->load($form_state->getValue('xntt_type'));
      if (empty($entity_type) || !$entity_type->isAnnotatable()) {
        $form_state->setErrorByName(
          'xntt_annotate',
          $this->t('The selected external entity type does not have annotations.')
        );
      }
    }
  }

  /**
   * Default submit method.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Inspect external entity fields and their mapping.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitInspectForm(array &$form, FormStateInterface $form_state) {
    $form_state->set('page', 'inspect');
    $xntt_type = $form_state->getValue('xntt_type');
    $xntt_id = $form_state->getValue('xntt_id');
    $url = Url::fromRoute(
      'xnttmanager.inspect',
      [
        'xntt_type' => $xntt_type,
        'xntt_id' => $xntt_id,
      ]
    );
    $form_state->setRedirectUrl($url);
  }

  /**
   * Submit method for batch processing.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitBatchForm(array &$form, FormStateInterface $form_state) {
    $form_state->set('page', 'batch');
    $params = [
      'xntt_type'  => $form_state->getValue('xntt_type'),
    ];

    // List actions to perform on entities.
    $actions = ['loading'];
    if ($form_state->getValue('xntt_save')) {
      $params['save'] = TRUE;
      $actions[] = 'saving';
    }

    if ($form_state->getValue('xntt_annotate')) {
      $params['annotate'] = TRUE;
      $actions[] = 'saving';
    }

    if (1 == count($actions)) {
      $title = 'External entity ' . $actions[0];
    }
    else {
      $title = ' and ' . array_pop($actions);
      $title = 'External entity ' . implode(', ', $actions) . $title;
    }

    $this->launchBatchProcessing(
      $form_state,
      $params,
      $title
    );
  }

  /**
   * Export external entity config.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitExportForm(array &$form, FormStateInterface $form_state) {
    $form_state->set('page', 'export');
    $xntt_type = $form_state->getValue('xntt_type');

    $entity_type = $this->entityTypeManager
      ->getStorage('external_entity_type')
      ->load($xntt_type);
    if ($entity_type === NULL) {
      throw new NotFoundHttpException();
    }

    // Process fields.
    $field_def_array = [];
    // Get field definitions.
    $field_def = $this
      ->entityFieldManager
      ->getFieldDefinitions($xntt_type, $xntt_type);
    foreach ($field_def as $field_id => $field_def) {
      // Ignore base fields.
      if (!$field_def instanceof BaseFieldDefinition) {
        $field_def_array = $field_def->toArray();
        // Manage entity ref.
        if (('entity_reference' == $field_def_array['field_type'])
            && (empty($field_def_array['settings']['handler']))
        ) {
          $field_def_array['settings']['handler'] = 'default';
        }
        $field_def_array[$field_id] = [
          'definition' => $field_def_array,
        ];
      }
    }

    // Get field storage definitions.
    $field_storage_def = $this
      ->entityFieldManager
      ->getFieldStorageDefinitions($xntt_type);
    foreach ($field_storage_def as $field_id => $field_storage_def) {
      // Only keep fields with a definition.
      if (!empty($field_def_array[$field_id])) {
        $field_def_array[$field_id]['storage'] = $field_storage_def->toArray();
      }
    }

    // Field mapper.
    try {
      $field_mapper_config = [
        'id' => $entity_type->getFieldMapperId(),
        'config' => $entity_type->getFieldMapperConfig(),
      ];
    }
    catch (PluginException $e) {
      $field_mapper_config = [];
    }

    // Storage client.
    try {
      $storage_config = [
        'id' => $entity_type->getStorageClientId(),
        'config' => $entity_type->getStorageClientConfig(),
      ];
    }
    catch (PluginException $e) {
      $storage_config = [];
    }

    $config_data = [
      'id' => $xntt_type,
      'date' => date('Ymd'),
      'label' => $entity_type->getLabel(),
      'label_plural' => $entity_type->getPluralLabel(),
      'description' => $entity_type->getDescription(),
      'read_only' => $entity_type->isReadOnly(),
      'generate_aliases' => $entity_type->automaticallyGenerateAliases(),
      'fields' => $field_def_array,
      'field_mapping' => $field_mapper_config,
      'storage' => $storage_config,
    ];

    // Convert to YAML.
    $yaml_content = Yaml::dump($config_data);
    $response = new Response($yaml_content);
    $disposition = HeaderUtils::makeDisposition(
      HeaderUtils::DISPOSITION_ATTACHMENT,
      $xntt_type . '-config.yml'
    );
    $response->headers->set('Content-Disposition', $disposition);

    $form_state->setResponse($response);

    $url = Url::fromRoute('xnttmanager.management');
    $form_state->setRedirectUrl($url);
    // @todo After submit, we stay on the same form page but buttons are
    // inactive. The page should be reloaded. Maybe rebuild form and perform
    // file creation on the building?
  }

  /**
   * {@inheritdoc}
   */
  public function validatePreImportForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $submit_button = $form_state->getTriggeringElement()['#name'];

    if (('import' == $submit_button) && empty($form_state->getValue('xntt_type'))) {
      $form_state->setErrorByName(
        'xntt_type',
        $this->t('You must select an external entity type.')
      );
    }

    if (!$form_state->getValue('xntt_config_file')) {
      $form_state->setErrorByName(
        'xntt_config_file',
        $this->t('You must provide an external entity config file.')
      );
    }

    // Get uploaded file content.
    $file_upload = file_save_upload(
      'xntt_config_file',
      ['file_validate_extensions' => ['yml yaml json']],
      FALSE,
      NULL,
      FileSystemInterface::EXISTS_RENAME
    );
    if (!empty($file_upload) && is_array($file_upload)) {
      /** @var \Drupal\file\Entity\File */
      $yaml_file = $file_upload[0];
      $yaml_file_uri = $yaml_file->getFileUri();
      if (!empty($yaml_file_uri)) {
        try {
          $yaml_data = Yaml::parseFile($yaml_file_uri);
          // Check config file content.
          $config_keys = [
            'id',
            'label',
            'label_plural',
            'description',
            'read_only',
            'generate_aliases',
            'fields',
            'field_mapping',
            'storage',
          ];
          $config_ok = TRUE;
          foreach ($config_keys as $config_key) {
            if (!array_key_exists($config_key, $yaml_data)) {
              $form_state->setErrorByName(
                'xntt_config_file',
                'Invalid config file: missing "' . $config_key . '" key.'
              );
              $config_ok = FALSE;
            }
          }
          if ($config_ok) {
            // Check if the external entity type already exists.
            if ('create_import' == $submit_button) {
              $new_entity_type = $this->entityTypeManager
                ->getStorage('external_entity_type')
                ->load($yaml_data['id']);
              if (!empty($new_entity_type)) {
                $form_state->setErrorByName(
                  'xntt_config_file',
                  'The external entity type to create ('
                  . $yaml_data['id']
                  . ') already exists. Import canceled.'
                );
                $config_ok = FALSE;
              }
            }

            if (!is_array($yaml_data['fields'])) {
              $form_state->setErrorByName(
                'xntt_config_file',
                'Invalid config file: invalid "fields" value.'
              );
              $config_ok = FALSE;
            }

            if (!is_array($yaml_data['field_mapping'])) {
              $form_state->setErrorByName(
                'xntt_config_file',
                'Invalid config file: invalid "field_mapping" value.'
              );
              $config_ok = FALSE;
            }
            elseif (!empty($yaml_data['field_mapping']['id'])) {
              $plid = $yaml_data['field_mapping']['id'];
              if (!$this->fieldMapperPluginManager->hasDefinition($plid)) {
                $form_state->setErrorByName(
                  'xntt_config_file',
                  'Invalid config file: field mapping plugin not found: "'
                  . $plid
                  . '"'
                );
                $config_ok = FALSE;
              }
            }

            if (!is_array($yaml_data['storage'])) {
              $form_state->setErrorByName(
                'xntt_config_file',
                'Invalid config file: invalid "storage" value.'
              );
              $config_ok = FALSE;
            }
            elseif (!empty($yaml_data['storage']['id'])) {
              $plid = $yaml_data['storage']['id'];
              if (!$this->storageClientPluginManager->hasDefinition($plid)) {
                $form_state->setErrorByName(
                  'xntt_config_file',
                  'Invalid config file: storage client plugin not found: "'
                  . $plid
                  . '"'
                );
                $config_ok = FALSE;
              }
            }
          }

          if ($config_ok) {
            $form_state->set('config_data', $yaml_data);
          }
        }
        catch (ParseException $e) {
          $err_line = $e->getParsedLine();
          if (0 <= $err_line) {
            $form_state->setErrorByName(
              'xntt_config_file',
              'Failed to parse Yaml config file near line '
              . $e->getParsedLine()
              . ': '
              . $e->getSnippet()
            );
          }
          else {
            $form_state->setErrorByName(
              'xntt_config_file',
              'Failed to parse Yaml config file: '
              . $config_data
            );
          }
        }
      }
      else {
        $form_state->setErrorByName(
          'xntt_config_file',
          $this->t('Unable to read uploaded file: empty file.')
        );
      }
    }
    else {
      $form_state->setErrorByName(
        'xntt_config_file',
        $this->t('Unable to read uploaded file. Check logs for details.')
      );
    }
  }

  /**
   * Prepare external entity config import.
   *
   * This is a two steps form.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitPreCreateImportForm(array &$form, FormStateInterface $form_state) {
    $form_state->set('page', 'create');

    $yaml_config = $form_state->get('config_data');

    $entity_type = $this->entityTypeManager
      ->getStorage('external_entity_type')
      ->create([
        'id' => $yaml_config['id'],
        'label' => $yaml_config['label'],
        'label_plural' => $yaml_config['label_plural'],
        'description' => $yaml_config['description'],
        'read_only' => $yaml_config['read_only'],
        'generate_aliases' => $yaml_config['generate_aliases'],
      ]);
    $entity_type->save();

    $form_state->set('xntt_type', $yaml_config['id']);
    $form_state->setValue('xntt_type', $yaml_config['id']);
    $form_state->setValue('field_config', TRUE);
    $form_state->setValue('mapping_config', TRUE);
    $form_state->setValue('storage_config', TRUE);

    $this->submitProcessImportForm($form, $form_state);
  }

  /**
   * Prepare external entity config import.
   *
   * This is a two steps form.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitPreImportForm(array &$form, FormStateInterface $form_state) {
    $form_state->set('page', 'import');
    $xntt_type = $form_state->getValue('xntt_type');

    $entity_type = $this->entityTypeManager
      ->getStorage('external_entity_type')
      ->load($xntt_type);
    if ($entity_type === NULL) {
      throw new NotFoundHttpException();
    }

    $form_state->set('xntt_type', $xntt_type);
    $form_state->setRebuild(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function buildImportForm(array $form, FormStateInterface $form_state) {

    $xntt_type = $form_state->get('xntt_type');

    $form['xntt_type_warning'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<b>WARNING:</b> the following existing external entity configuration will be replaced (cannot be undone): <em>@type</em>', ['@type' => $xntt_type]),
      '#prefix' => '<div class="xnttmanager-warning">',
      '#suffix' => '</div>',
      '#attached' => [
        'library' => [
          'xnttmanager/xnttmanager',
        ],
      ],
    ];

    $form['xntt_type'] = [
      '#type' => 'hidden',
      '#value' => $xntt_type,
    ];

    $form['field_remove_unused'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Remove unused fields'),
      '#description' => $this->t('If checked, all fields not present in config will be removed.'),
      '#default_value' => FALSE,
    ];

    $form['field_config'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Import fields'),
      '#description' => $this->t('If checked, all missing fields will be added and configured.'),
      '#default_value' => TRUE,
    ];

    $form['mapping_config'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Import field mapping'),
      '#default_value' => TRUE,
    ];

    $form['storage_config'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Import storage config'),
      '#default_value' => TRUE,
    ];

    $form['actions']['import'] = [
      '#type' => 'submit',
      '#name' => 'import',
      '#value' => $this->t('Import'),
      '#submit' => [[$this, 'submitProcessImportForm']],
    ];

    $form['actions']['back'] = [
      '#type' => 'submit',
      '#name' => 'back',
      '#value' => $this->t('Back'),
      '#submit' => [[$this, 'submitCancelImportForm']],
    ];

    return $form;
  }

  /**
   * Cancel import operation.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitCancelImportForm(array &$form, FormStateInterface $form_state) {
    $form_state->set('page', 'done');
    $form_state->setRebuild(TRUE);
  }

  /**
   * Import external entity config.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitProcessImportForm(array &$form, FormStateInterface $form_state) {
    $form_state->set('page', 'done');
    $yaml_config = $form_state->get('config_data');
    $xntt_type = $form_state->get('xntt_type');
    $entity_type = $this->entityTypeManager
      ->getStorage('external_entity_type')
      ->load($xntt_type);
    if ($entity_type === NULL) {
      throw new NotFoundHttpException();
    }

    // Process properties.
    if (isset($yaml_config['label'])) {
      $entity_type->set('label', $yaml_config['label']);
    }
    if (isset($yaml_config['label_plural'])) {
      $entity_type->set('label_plural', $yaml_config['label_plural']);
    }
    if (isset($yaml_config['description'])) {
      $entity_type->set('description', $yaml_config['description']);
    }
    if (isset($yaml_config['read_only'])) {
      $entity_type->set('read_only', $yaml_config['read_only']);
    }
    if (isset($yaml_config['generate_aliases'])) {
      $entity_type->set('generate_aliases', $yaml_config['generate_aliases']);
    }

    if ($form_state->getValue('field_remove_unused')) {
      $removed_fields = [];
      // Remove unused fields.
      $field_def = $this
        ->entityFieldManager
        ->getFieldDefinitions($xntt_type, $xntt_type);
      foreach ($field_def as $field_id => $field_def) {
        // Ignore base fields.
        if ((!$field_def instanceof BaseFieldDefinition)
            && (!array_key_exists($field_id, $yaml_config['fields']))
        ) {
          $removed_fields[] = $field_id;
          // Remove field (associated field storage is automatically removed).
          $field_def->delete();
        }
      }
      if (!empty($removed_fields)) {
        $this->messenger(
          $this->t(
            'The following fields were removed: @fields',
            ['@fields' => implode(', ', $removed_fields)]
          )
        );
      }
    }

    if ($form_state->getValue('field_config')) {
      // Add and configure missing fields.
      $field_storage_man = $this
        ->entityTypeManager
        ->getStorage('field_storage_config');
      $field_config_man = $this
        ->entityTypeManager
        ->getStorage('field_config');
      try {
        foreach ($yaml_config['fields'] as $field_name => $field_config) {
          if (empty($field_config['definition'])
            || !is_array($field_config['definition'])
            || empty($field_config['definition']['field_type'])
            || empty($field_config['storage'])
            || !is_array($field_config['storage'])
            || empty($field_config['storage']['type'])
          ) {
            // Skip incomplete config.
            $this->logger('xnttmanager')->warning(
              'Failed importing field "'
              . $field_name
              . '" for external entity "'
              . $xntt_type
              . '": incomplete config.'
            );
            continue;
          }

          // Make sure we got the corresponding field plugin.
          $field_type = $field_config['definition']['field_type'];
          $field_storage_type = $field_config['storage']['type'];
          if (!$this->fieldTypePluginManager->hasDefinition($field_type)) {
            // Skip missing plugins.
            $this->logger('xnttmanager')->warning(
              'Failed importing field "'
              . $field_name
              . '" for external entity "'
              . $xntt_type
              . '": field plugin "'
              . $field_type
              . '" missing.'
            );
            continue;
          }
          elseif (!$this->fieldTypePluginManager->hasDefinition($field_storage_type)) {
            // Skip missing plugins.
            $this->logger('xnttmanager')->warning(
              'Failed importing field "'
              . $field_name
              . '" for external entity "'
              . $xntt_type
              . '": field storage plugin "'
              . $field_type
              . '" missing.'
            );
            continue;
          }

          // Cleanup config.
          unset($field_config['storage']['uuid']);
          $field_config['storage']['id'] = "$xntt_type.$field_name";
          unset($field_config['definition']['uuid']);
          $field_config['definition']['id'] = "$xntt_type.$xntt_type.$field_name";

          // Storage first.
          $field_storage_def = $field_storage_man->load($field_config['storage']['id']);
          if (empty($field_storage_def)) {
            // Not existing, create.
            // Dependencies is auto-set by Drupal on save.
            $def_array = [
              'entity_type' => $xntt_type,
              'bundle' => $xntt_type,
              'field_name' => $field_name,
              'dependencies' => [],
            ]
              + $field_config['storage'];
            $field_storage_def = $field_storage_man
              ->create($def_array)
              ->save();
          }

          $fdef['id'] =
          $field_def = $field_config_man->load($field_config['definition']['id']);
          if (empty($field_def)) {
            // Not existing, create.
            // Dependencies is auto-set by Drupal on save.
            $def_array = [
              'entity_type' => $xntt_type,
              'bundle' => $xntt_type,
              'dependencies' => [],
            ]
              + $field_config['definition'];
            $field_config_man
              ->create($def_array)
              ->save();
          }
        }
      }
      catch (PluginNotFoundException $e) {

      }
    }

    if ($form_state->getValue('mapping_config')) {
      // Set field mapper config.
      $entity_type->setFieldMapperId($yaml_config['field_mapping']['id']);
      $entity_type->setFieldMapperConfig($yaml_config['field_mapping']['config']);
    }

    if ($form_state->getValue('storage_config')) {
      // Set storage client config.
      $entity_type->setStorageClientId($yaml_config['storage']['id']);
      $entity_type->setStorageClientConfig($yaml_config['storage']['config']);
    }

    // Savec changes.
    $entity_type->save();

    $this->messenger(
      $this->t(
        'Successfully imported external entity config for type @type',
        ['@type' => $xntt_type]
      )
    );

    $form_state->setRebuild(TRUE);
  }

  /**
   * Batch-process all external entities of the given type.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   * @param array $params
   *   The batch parameters.
   * @param string $title
   *   The batch title.
   */
  public function launchBatchProcessing(
    FormStateInterface $form_state,
    array $params,
    string $title
  ) {
    $operations = [
      [
        'xnttmanager_bulk_process',
        [$params],
      ],
    ];
    $batch = [
      // phpcs:disable
      // Users may want to use translated titles.
      'title' => $this->t($title),
      // phpcs:enable
      'operations' => $operations,
      'finished' => 'xnttmanager_bulk_finished',
      'progressive' => TRUE,
    ];
    batch_set($batch);
  }

}
