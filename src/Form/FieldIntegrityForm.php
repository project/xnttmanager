<?php

namespace Drupal\xnttmanager\Form;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class FieldIntegrityForm.
 *
 * Provides a simple form to inspect external entity field integrity.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class FieldIntegrityForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->entityFieldManager = $container->get('entity_field.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $xntt_types = xnttmanager_get_external_entity_type_list();
    $incorrect_fields = [
      'dependency' => [],
      'config' => [],
    ];
    $field_config_man = $this->entityTypeManager->getStorage('field_config');
    foreach ($xntt_types as $xntt_type) {
      // Get field definitions.
      $field_def = $this
        ->entityFieldManager
        ->getFieldDefinitions($xntt_type, $xntt_type);
      foreach ($field_def as $field_id => $field_def) {
        // Ignore base fields.
        if (!$field_def instanceof BaseFieldDefinition) {
          $field_config_id = $xntt_type . '.' . $xntt_type . '.' . $field_id;
          $field_config = $field_config_man->load($field_config_id);
          if (empty($field_config)) {
            $incorrect_fields['config'][] = $field_config_id;
          }
          else {
            $conf = $field_config->getDependencies();
            $dependency = 'external_entities.external_entity_type.' . $xntt_type;
            if (!in_array($dependency, $conf['config'])) {
              $incorrect_fields['dependency'][] = $field_config_id;
            }
          }
        }
      }
    }

    if (empty($incorrect_fields['dependency']) && empty($incorrect_fields['config'])) {
      $form['field_ok'] = [
        '#type' => 'markup',
        '#markup' => '<p>No field problem detected.</p>',
      ];
    }
    else {
      if (!empty($incorrect_fields['dependency'])) {
        $form['missing_dep'] = [
          '#type' => 'markup',
          '#markup' =>
          "<p>\n"
          . $this->t('Fields with missing dependencies')
          . "<ul>\n  <li>"
          . implode("</li>\n  <li>", $incorrect_fields['dependency'])
          . "  </li>\n</ul>\n</p>\n",
        ];
      }
      if (!empty($incorrect_fields['config'])) {
        $form['missing_conf'] = [
          '#type' => 'markup',
          '#markup' =>
          "<p>\n"
          . $this->t('Fields with missing config')
          . "<ul>\n  <li>"
          . implode("</li>\n  <li>", $incorrect_fields['config'])
          . "  </li>\n</ul>\n</p>\n",
        ];
      }

      $form['actions']['fix'] = [
        '#type' => 'submit',
        '#name' => 'fix_fields',
        '#value' => $this->t('Fix fields'),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xnttmanager_field_integrity_form';
  }

  /**
   * Default submit method.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $fixed = 0;
    $failed = 0;
    $xntt_types = xnttmanager_get_external_entity_type_list();
    $field_config_man = $this->entityTypeManager->getStorage('field_config');
    foreach ($xntt_types as $xntt_type) {
      // Get field definitions.
      $field_def = $this
        ->entityFieldManager
        ->getFieldDefinitions($xntt_type, $xntt_type);
      foreach ($field_def as $field_id => $field_def) {
        // Ignore base fields.
        try {
          if (!$field_def instanceof BaseFieldDefinition) {
            $field_config_id = $xntt_type . '.' . $xntt_type . '.' . $field_id;
            $field_config = $field_config_man->load($field_config_id);
            if (empty($field_config)) {
              // @todo Create field config.
              ++$failed;
            }
            else {
              $conf = $field_config->getDependencies();
              $dependency = 'external_entities.external_entity_type.' . $xntt_type;
              if (!in_array($dependency, $conf['config'])) {
                // Saving will recompute dependencies.
                $field_config->save();
              }
            }
          }
          ++$fixed;
        }
        catch (\Error) {
          ++$failed;
        }
      }
    }

    $this->messenger(
      $this->t(
        'The @fixed fields were fixed. Failed to fix @failed fields.',
        [
          '@fixed' => $fixed,
          '@failed' => $failed,
        ]
      )
    );

  }

}
